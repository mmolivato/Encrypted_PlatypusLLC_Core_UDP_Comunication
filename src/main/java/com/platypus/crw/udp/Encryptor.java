package com.platypus.crw.udp;

import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This Class provide a solution for fast UDP data encryption
 * using AES 128 / CBC / PKCS5PADDING.
 * 
 * @author Matteo Olivato <matteo.olivato@studenti.univr.it>
 * @author Federico Bianchi <federico.bianchi@studenti.univr.it>
 *
 */
public class Encryptor {

	// Initialization Vector
	private byte[] ivbytes;
	
	// Secret key
	private SecretKeySpec skeySpec;
	// Chipher
	private Cipher cipher;
	// Secure random bytes generator
	private SecureRandom sr;
	
	// Initialization Vector length
	private static final int ivlength = 16;

	/**
	 * Builder using the default key for AES 128 / CBC / PKCS5PADDING encryption
	 */
	public Encryptor(){
		// builds the encryptor with default value
		this("PlatypusLLC54321");
	}
	
	/**
	 * Parametric builder that allows custom key initialization
	 * 
	 * @param key the security key used for encryption
	 */
	public Encryptor(String key) {

		sr = new SecureRandom();
		ivbytes = new byte[ivlength];

		// First IV initialization
		sr.nextBytes(ivbytes);

		try {
			// create a new SecretKeySpec from key using AES as cipher algorithm
			skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			// generate an instance of the desired cipher
			cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Encrypts data using AES 128 / CBC / PKCS5PADDING.
	 * @param data arraybytes to encrypt
	 * @return arraybytes of data encrypted
	 */
	private byte[] encrypt(byte[] data) {
		try {
			// inits the cipher using the correct initialization vector
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(ivbytes));
			
			// returns the encrypted data bytes
			return cipher.doFinal(data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Decrypts data using AES 128 / CBC / PKCS5PADDING
	 * @param encrypted the encrypted data
	 * @param ivbytes initialization vector
	 * @return decrypted data
	 */
	private byte[] decrypt(byte[] encrypted, byte[] ivbytes) {
		try {
			// inits the cipher using the correct initialization vector
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(ivbytes));
			
			// returns the decrypted data bytes
			return cipher.doFinal(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
			// return encrypted array when are problems with decryption
			// this can be caused by packet modifications during routing
			return encrypted;
		}
	}
	
	/**
	 * Encrypts a message using AES 128 / CBC / PKCS5PADDING and returns a concatenated message.
	 * The first part will be the initialization vector,
	 * the second part will be the encrypted message.
	 * @param message the original message to encrypt
	 * @return the concatenation of initialization vector and the encrypted data 
	 */
	public byte[] encryptMessage(byte[] message){
		synchronized(this){
	        byte[] encrypted_data = encrypt(message);
			
			// Create the result data with the ivbytes and encrypted data join
			byte[] result_data = new byte[ivlength + encrypted_data.length];
			System.arraycopy(ivbytes, 0, result_data, 0, ivlength);
			System.arraycopy(encrypted_data, 0, result_data, ivlength, encrypted_data.length);
			
			// after cipher initialization updates the IV with random bytes
			sr.nextBytes(ivbytes);

	        return result_data;
		}
	}
	
	/**
	 * Decrypts a message extracting the initialization vector from the first 16 bytes
	 * and decrypting the results data using AES 128 / CBC / PKCS5PADDING.
	 * @param message concatenated encrypted message
	 * @param message_length length of the encrypted message
	 * @return decrypted message
	 */
	public byte[] decryptMessage(byte[] message, int message_length){
		synchronized(this){
			// extracts the initialization vector
			byte[] ivbytes = Arrays.copyOfRange(message, 0, ivlength);
			// extracts the encrypted data
			byte[] encrypted = Arrays.copyOfRange(message, ivlength, message_length);
			
			// returns the correct decrypted message using its IV
			return decrypt(encrypted, ivbytes);
		}
	}
}